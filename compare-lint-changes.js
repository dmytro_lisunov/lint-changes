'use strict';

const logger = require('./logger');

async function checkCommitErrors(lintResultArr, changedFilesDataArr) {
    if (!(Array.isArray(lintResultArr) && lintResultArr.length)) {
        logger.warn(`No lint results. Check your linting Configuration`);
        return;
    }
    if (!(Array.isArray(changedFilesDataArr) && changedFilesDataArr.length)) {
        logger.warn(`No changes are passed. Check "git diff ..." command`);
        return;
    }
    let lintErrorsArr = changedFilesDataArr.reduce( (lintErrorsArr, changedFile) => {
        let lintedFile = lintResultArr.find( (lintedFile) => {
            return (lintedFile.source === changedFile.source);
        });
        if (lintedFile) {
            let errors = [];
            changedFile.changedLines.forEach(line => {
                errors = errors.concat( lintedFile.warnings.filter(error => (error.line >= line.start && error.line <= line.end)) );
            });
            if (errors.length) {
                lintErrorsArr.push({
                    source : changedFile.source,
                    warnings: errors
                });
            }
        }
        return lintErrorsArr;
    }, []);
    return lintErrorsArr.length ? lintErrorsArr : false;
}

module.exports = checkCommitErrors;