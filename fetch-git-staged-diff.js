'use strict';

// Node.js internal modules
const util = require('util');
const process = require('process');
const exec = util.promisify(require('child_process').exec);

// Third-party NPM modules
const parseDiff = require('parse-diff');

// Package custom modules
const logger = require('./logger');

async function fetchGitStagedDiffArr(cwd = process.cwd()) {
    let gitDiffResultArr = null;
    const executableCommand = 'git diff --diff-filter=d --cached --unified=0';
    try {
        let {
            stdout,
            stderr
        } = await exec(executableCommand, {
            cwd: cwd
        });
        if (stderr) {
            logger.error(stderr);
        } else if (!stdout) {
            logger.log(`No staged are present`);
        } else {
            gitDiffResultArr = parseDiff(stdout);
        }
    } catch (error) {
        logger.error(`Node.js could not execute command [${executableCommand}]`);
        logger.error(error.stack);
    }
    return gitDiffResultArr;
}

module.exports = fetchGitStagedDiffArr;