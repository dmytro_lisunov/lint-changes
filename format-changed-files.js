'use sctrict';

// Node.js internal modules
const process = require('process');
const path = require('path');
const fs = require('fs');

// Package custom modules
const logger = require('./logger');

const defaultOptions = {
    useAbsolutePaths: true,
    cwd: process.cwd()
};

async function formatChangedFilesData(diffArr, options) {
    let opts = Object.assign({}, defaultOptions, options);
    let changedFilesArr = null;
    if (Array.isArray(diffArr) && diffArr.length ) {
        changedFilesArr = [];
        diffArr.forEach(file => {
            let source = opts.useAbsolutePaths ? path.join(opts.cwd, '..', file.to) : file.to;
            source = source.replace(/\\/g, '/');
            if (fs.existsSync(source)) {
                changedFilesArr.push({
                    source: source,
                    changedLines: file.chunks.map(chunk => {
                        return {
                            start : chunk.newStart,
                            end : chunk.newStart + chunk.newLines
                        }
                    })
                });
            } else {
                logger.warn(`File [${source}] does not exist`);
            }
        });
    } else {
        logger.warn(`diffArr is not defined`);
    }
    return changedFilesArr;
}

module.exports = formatChangedFilesData;