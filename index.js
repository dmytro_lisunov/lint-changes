'use sctrict';

// Node.js internal modules
const process = require('process');

// Third-party NPM modules
var fileMatch = require('file-match');

// Package custom modules
const logger = require('./logger');
const fetchGitStagedDiffArr = require('./fetch-git-staged-diff');
const formatChangedFilesData = require('./format-changed-files');
const compareLintChanges = require('./compare-lint-changes');

async function lintCganges (linters = require('./linters')) {
    try {
        const diffArr = await fetchGitStagedDiffArr();
        if (diffArr && diffArr.length) {
            const changedFilesDataArr = await formatChangedFilesData(diffArr);
            if (Array.isArray(changedFilesDataArr) && changedFilesDataArr.length) {
                let filePaths = changedFilesDataArr.map((file) => file.source);
                let lintResultsArr = await Promise.all(linters.map( (linter) => {
                    if (typeof(linter.lint) === "function") {
                        let matchedFiles = filePaths.filter(fileMatch(linter.filePattern));
                        let matchedChangedFilesArr = changedFilesDataArr
                            .filter( (file) => matchedFiles
                                .find( (matchedFile) => (matchedFile === file.source) )
                            );
                        return linter.lint(matchedFiles).then( (lintResultArr) => {
                            return compareLintChanges(lintResultArr, matchedChangedFilesArr);
                        });
                    } else {
                        logger.warn(`The linter "${linter.name || String(linter)}" does not support "lint()" method`);
                        return false;
                    }
                }));
                lintResultsArr.forEach( (result) => {
                    const util = require('util');
                    if (Array.isArray(result)) {
                        result.forEach(error => logger.info(util.inspect(error)) )
                    } else {
                        logger.info(util.inspect(result))
                    }
                });
            } else {
                logger.warn(`There is no readable files for linting`);
            }
        } else {
            logger.info(`There are no changes detected by Git in ${process.cwd()}`);
        }
    } catch (error) {
        logger.error(error);
    }
}

module.exports = lintCganges();