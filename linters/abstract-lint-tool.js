'use sctrict';

// Node.js internal modules
const process = require('process');
const path = require('path');
const fs = require('fs');

class LintingTool {
    constructor(options = {}, name = 'linter') {
        this.options = options;
        this.name = name;
        this.filePattern = '**/*.*';
        this.defaultOptions = {};
    }

    async lint(filePathsArr = []){
        console.log(`Abstract "lint()" method was invoked on linter [${this.name}]`);
        return 'No errors';
    }

    resolveConfigPath(configName) {
        let configExtensions = ['', '.json', 'yaml', '.js'];
        let configFilePath = null;
        for (let ext of configExtensions) {
            let testPath = path.join(process.cwd(), configName + ext);
            let pathIsExist = fs.existsSync(testPath);
            if (pathIsExist) {
                configFilePath = testPath;
                break;
            }
        }
        return configFilePath;
    }
}

module.exports = LintingTool;