'use sctrict';

// Third-party NPM modules
const Linter = require('eslint').CLIEngine;
const eslintConfig = require('tlg-lint-rules').eslint;

// Package custom modules
const logger = require('./../logger');
const LintingTool = require('./abstract-lint-tool');




class EsLinterTool extends LintingTool {

    constructor(options = {}, name = 'ESlint') {
        super(options, name);
        let config = this.resolveConfigPath('.eslintrc') || eslintConfig;
        this.filePattern = '**/*.js';
        this.defaultOptions = {
            useEslintrc: true,
            configFile: config
        }
    }

    async lint(filePathsArr) {
        if ( !(Array.isArray(filePathsArr) && filePathsArr.length) ) {
            logger.warn(`There are no files passed to linter [${this.name}]`);
            return null;
        }
        this.options = Object.assign({}, this.defaultOptions, this.options);
        const eslint = new Linter(this.options);
        logger.info(`Linter [${this.name}] is using config ${this.options.configFile}`);
        let lintResult = 'No errors';
        try {
            let eslintOutputObj = eslint.executeOnFiles(filePathsArr);
            if (eslintOutputObj.errorCount > 0) {
                lintResult = eslintOutputObj.results.map( (file) => {
                    file.source = file.filePath.replace(/\\/g, '/');
                    file.warnings = file.messages;
                    return file;
                });
            }
        } catch (error) {
            logger.error(`Unable to execute [${this.name}]`);
            logger.error(error);
        }
        return lintResult;
    }
}

module.exports = new EsLinterTool();