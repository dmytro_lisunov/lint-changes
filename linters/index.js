'use strict';

const styleLinter = require('./stylelint-tool');
const esLinter = require('./eslint-tool');

module.exports = [
    styleLinter,
    esLinter
];