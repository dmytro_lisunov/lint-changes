'use sctrict';

// Third-party NPM modules
const stylelint = require('stylelint');
const stylelintConfig = require('tlg-lint-rules').stylelint;

// Package custom modules
const logger = require('./../logger');
const LintingTool = require('./abstract-lint-tool');




class StyleLinterTool extends LintingTool {

    constructor(options = {}, name = 'stylelint') {
        super(options, name);
        let configFile = this.resolveConfigPath('.stylelintrc') || stylelintConfig;
        this.filePattern = '**/*.scss';
        this.defaultOptions = {
            configFile: configFile,
            formatter: "json",
            syntax: 'scss',
            files: [],
            //code: 'a { color: pink; }' // For testing 'configFile' path.
            // Uncomment it and comment 'files' property here AND 'this.options.files' in 'lint()' method
        }
    }

    async lint(filePathsArr) {
        if ( !(Array.isArray(filePathsArr) && filePathsArr.length) ) {
            logger.warn(`There are no files passed to linter [${this.name}]`);
            return null;
        }
        this.options.files = filePathsArr;
        this.options = Object.assign({}, this.defaultOptions, this.options);
        logger.info(`Linter [${this.name}] is using config ${this.options.configFile}`);
        let lintResult = 'No errors';
        try {
            let { errored, output } = await stylelint.lint(this.options);
            if (errored) {
                lintResult = JSON.parse(output).map( (file) => {
                    file.source = file.source.replace(/\\/g, '/');
                    return file;
                });
            }
        } catch (error) {
            logger.error(`Unable to execute [${this.name}]`);
            logger.error(error);
        }
        return lintResult;
    }
}

module.exports = new StyleLinterTool();